This XSLT stylesheet will convert your Android device's SMS and call history backups created with [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore) to a JSON file which can be imported on any Mer / Jolla / Sailfish OS device with **commhistory-tool** installed.

# Usage
* Install **SMS Backup & Restore** on your Android device and create a backup of the desired data.
* Copy the resulting files (sms-*timestamp*.xml / calls-*timestamp*.xml) and the file *android-sms-backup-restore-2-jolla-commhistory-tool.xsl* to your Mer / Jolla / Sailfish device. (Hint: Just use the root of the *Mass storage* device, otherwise you will have to change to the target directory later yourself.)
* Open the **Terminal** app.
* Execute `xsltproc android-sms-backup-restore-2-jolla-commhistory-tool.xsl sms-*.xml > sms.json` (SMS) and / or `xsltproc android-sms-backup-restore-2-jolla-commhistory-tool.xsl calls-*.xml > calls.json` (call history) to convert the files.
* Execute `commhistory-tool import-json sms.json` for importing your SMS and / or `commhistory-tool import-json calls.json` for importing your call history.

# Limitations
The following limitations are caused by commhistory-tool's JSON importer:
* It's not possible to import MMS messages.
* For SMS the types *Draft*, *Outbox*, *Failed* and *Queued* are not supported; such messages will just be skipped. (FYI: I wasn't able to get **SMS Backup & Restore** to actually export such messages anyway.)
* For calls the types *Missed*, *Voicemail*, *Rejected* and *Refused List* will all be imported as *missed*.

# References
* [SMS Backup & Restore XML file documentation](https://synctech.com.au/sms-backup-restore/fields-in-xml-backup-files/)
* [commhistory-tool source code](https://git.merproject.org/mer-core/libcommhistory/blob/master/tools/commhistory-tool.cpp) - see `doJsonImport` for a list of importable fields
* [sailfish-msg-importer](https://github.com/javaes/sailfish-msg-importer) - alternative project which is directly modifying the database instead of using **commhistory-tool**; does not support all fields
