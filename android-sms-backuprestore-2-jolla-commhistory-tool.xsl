<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="http://exslt.org/dates-and-times" xmlns:func="http://exslt.org/functions" xmlns:str="http://exslt.org/strings" xmlns:local="http://example.org" extension-element-prefixes="func">
<xsl:output method="text" />

<xsl:template match="/">[<xsl:apply-templates select="/smses" /><xsl:apply-templates select="/calls" />
]
</xsl:template>

<func:function name="local:getisodate">
	<xsl:param name="timestamp" />
	<func:result select="date:add('1970', concat('PT', substring($timestamp, 1, 10), '.', substring($timestamp, 11), 'S'))" />
</func:function>
<func:function name="local:escapejson">
	<xsl:param name="string" />
	<func:result select="str:replace(str:replace(str:replace($string, '\', '\\'), '&#10;', '\n'), '&quot;', '\&quot;')" />
</func:function>

<!-- Only Received and Sent messages are supported by the JSON importer, so skip everything else (Draft, Outbox, Failed, Queued) -->
<xsl:template match="smses">
	<xsl:variable name="output">
		<xsl:for-each select="sms"><xsl:choose><xsl:when test="@type='1' or @type='2'">
	{
		"type": "sms",
		"to": "<xsl:value-of select="@address" />",
		"events": [
			{
				"direction": "<xsl:choose><xsl:when test="@type='1'">in</xsl:when><xsl:when test="@type='2'">out</xsl:when></xsl:choose>",
				"date": "<xsl:value-of select="local:getisodate(@date)" />",
				"unread": <xsl:choose><xsl:when test="@read='0'">true</xsl:when><xsl:when test="@read='1'">false</xsl:when></xsl:choose>,
				"text": "<xsl:value-of select="local:escapejson(@body)" />"
			}
		]
	},</xsl:when></xsl:choose>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="substring($output, 1, (string-length($output) - 1))" />
</xsl:template>

<!-- "Missed", "Voicemail", "Rejected" and "Refused List" types will be merged into a simple "missed" -->
<xsl:template match="calls">
	<xsl:variable name="output">
		<xsl:for-each select="call">
	{
		"type": "call",
		"to": "<xsl:value-of select="@number" />",
		"events": [
			{
				"direction": "<xsl:choose><xsl:when test="@type='2'">out</xsl:when><xsl:otherwise>in</xsl:otherwise></xsl:choose>",
				"date": "<xsl:variable name="isodate" select="local:getisodate(@date)" /><xsl:value-of select="$isodate" />",
				"endDate": "<xsl:value-of select="date:add($isodate, concat('PT', @duration, 'S'))" />",
				"missed": <xsl:choose><xsl:when test="@type='1' or @type='2'">false</xsl:when><xsl:otherwise>true</xsl:otherwise></xsl:choose>
			}
		]
	},</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="substring($output, 1, (string-length($output) - 1))" />
</xsl:template>

</xsl:stylesheet>
